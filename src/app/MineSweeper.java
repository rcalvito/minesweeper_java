package app;

import com.sun.javafx.scene.control.skin.LabeledText;
import controllers.Controller;
import javafx.application.Application;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Main class of Minesweeper, controls the flow of the game.
 * @author Rodrigo Calvo
 * @version 1.0
 */
public class MineSweeper extends Application {
    //variables needed for the game
    private Image flag = new Image(getClass().getResourceAsStream("/images/flag.png"));
    private Image question = new Image(getClass().getResourceAsStream("/images/questionMark.png"));
    private Image bomb = new Image(getClass().getResourceAsStream("/images/bomb.png"));
    private ArrayList<ArrayList<Tile>> tiles = new ArrayList<>();
    private double height;
    private double width;
    private Timer timer;
    private int time=0;
    private int count;
    EventHandler<MouseEvent> pressed;
    EventHandler<MouseEvent> released;


    @Override
    public void start(Stage primaryStage) throws Exception {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("../layout/layout.fxml")); //Loads FXML file
        Parent root = loader.load();
        Controller controller= loader.getController(); // Gets controller.
        create2dArray();  // Creates 2D array.

        //Mouse event for mouse released.
        released = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if(event.getTarget() instanceof ImageView || event.getTarget() instanceof LabeledText || event.getTarget() instanceof Label){
                    //Notihng, avoids error
                }
                else if(((Rectangle) event.getTarget()).getFill()== Color.RED){

                    timer.cancel();
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setHeaderText(null);
                    alert.setContentText("You Lost\n" + "It took you: " + time + " seconds");
                    alert.show();
                    time=0;
                    markBombs();
                    controller.getPlayBtn().setDisable(false);
                    controller.getTilePane().setOnMouseReleased(null);
                    setPressedNull();
                }
                else if(((Rectangle) event.getTarget()).getFill()== Color.LIGHTBLUE){
                    int i = (int) (event.getX()/23);
                    int j = (int) (event.getY()/23);
                    checkTiles(j,i,controller);
                }
                checkFlags(controller);
            }
        };

        // Sets up the stage.
        primaryStage.setTitle("Minesweeperish");
        primaryStage.setScene(new Scene(root, 460, 505));
        primaryStage.setResizable(false);
        primaryStage.show();
        primaryStage.setOnCloseRequest(event -> {
            if(timer!=null){
                timer.cancel();
            }
        });
        controller.getTilePane().setOnMouseReleased(null);


        height=controller.getTilePane().getHeight()/20;
        width=controller.getTilePane().getWidth()/20;

        createBoard(controller); // Creates Board.
        setPressedNull(); // Sets mouse pressed to null so nothing happens before game starts.


        //Start button event handler.
        controller.getPlayBtn().setOnAction((event -> {
            controller.getTilePane().setOnMouseReleased(released);
            count=0;
            clearBoard(controller);// clears board
            createBoard(controller); // creates a new one.
            controller.getPlayBtn().setDisable(true); //Disables start button
            // New timer starts for each game.
            timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    time++;
                    Platform.runLater(()->changeTime(controller));
                }
            },0,1000);
        }));

    }

    /**
     * Method to create 2d array.
     */
    private void create2dArray(){
        for(int i=0;i<20;i++){
            tiles.add(new ArrayList<>());
        }

    }

    /**
     * Creates board and randomizes bombs
     * @param controller
     */
    private void createBoard(Controller controller){
        ArrayList<Tile> temp = new ArrayList<>();
        for(int i=0;i<400;i++){
            if (i < 100) {
                temp.add(new Tile(height, width, flag, question, true));
            } else {
                temp.add(new Tile(height, width, flag, question, false));
            }
        }
        Collections.shuffle(temp);
        int k=0;
        for(int i=0;i<20;i++){
            for(int j=0;j<20;j++) {
                tiles.get(i).add(temp.get(k));
                controller.getTilePane().getChildren().add(temp.get(k));
                k++;
            }
        }
        setNumbers();
    }

    /**
     * Clears the board.
     * @param controller
     */
    private void clearBoard(Controller controller){
        for(int i=0;i<20;i++){
            for(int j=0;j<20;j++){
                controller.getTilePane().getChildren().remove(tiles.get(i).get(j));
            }
        }
        tiles.clear();
        create2dArray();
    }

    /**
     * Sets timer label to the actual time.
     * @param controller
     */
    private void changeTime(Controller controller){
        controller.getTimer().setText(String.valueOf(time));
    }

    /**
     * Recursive function to clear neighboor cells if they don't have bombs.
     * @param i i position of tile
     * @param j j position of tile
     * @param controller
     */
    private void checkTiles(int i, int j,Controller controller){

        if (i<0 || j<0 || i>=tiles.size() || j>=tiles.size()) {
            return;
        }
        if(tiles.get(i).get(j).getPressed() || tiles.get(i).get(j).getBomb()){
            return;
        }
        if(!tiles.get(i).get(j).hasLabel() && !tiles.get(i).get(j).getPressed()){
            tiles.get(i).get(j).gotPressed();
            count++;
            checkWon(controller);
            return;
        }
        if(!tiles.get(i).get(j).getPressed()){
            tiles.get(i).get(j).gotPressed();
            count++;
            checkWon(controller);
        }
        checkTiles(i + 1, j,controller);
        checkTiles(i - 1, j,controller);
        checkTiles(i, j + 1,controller);
        checkTiles(i, j - 1,controller);
        checkTiles(i - 1, j - 1,controller);
        checkTiles(i - 1, j + 1,controller);
        checkTiles(i + 1, j + 1,controller);
        checkTiles(i + 1, j - 1,controller);

    }

    /**
     * Marks Bombs to either green red or yellow depending on the case.
     */
    private void markBombs(){
        for(int i=0;i<20;i++){
            for(int j=0;j<20;j++){
                Tile temp=tiles.get(i).get(j);
                if(temp.getBomb() && temp.hasImage()){
                    temp.turnGreen();
                }
                else if(temp.getBomb() && !temp.hasImage()){
                    ImageView bombPic = new ImageView(bomb);
                    bombPic.setFitWidth(23);
                    bombPic.setFitHeight(23);
                    temp.turnRed();
                    temp.getChildren().add(bombPic);

                }
                else if(!temp.getBomb() && temp.hasImage()){
                    temp.turnYellow();
                }
            }
        }
    }

    /**
     * Sets the number of neighboor cells with bombs on each tile.
     */
    private void setNumbers(){
        for(int i=0;i<20;i++){
            int count=0;
            for(int j=0;j<20;j++){
                int k=i-1;
                int l=j-1;
                if(!(k<0 || l<0 || k>=tiles.size() || l>=tiles.size()) && tiles.get(k).get(l).getBomb()){
                    count++;
                }
                k=i+1;
                l=j-1;
                if(!(k<0 || l<0 || k>=tiles.size() || l>=tiles.size()) && tiles.get(k).get(l).getBomb()){
                    count++;
                }
                k=i+1;
                l=j+1;
                if(!(k<0 || l<0 || k>=tiles.size() || l>=tiles.size()) && tiles.get(k).get(l).getBomb()){
                    count++;
                }
                k=i-1;
                l=j+1;;
                if(!(k<0 || l<0 || k>=tiles.size() || l>=tiles.size()) && tiles.get(k).get(l).getBomb()){
                    count++;
                }
                k=i;
                l=j-1;
                if(!(k<0 || l<0 || k>=tiles.size() || l>=tiles.size()) && tiles.get(k).get(l).getBomb()){
                    count++;
                }
                k=i;
                l=j+1;
                if(!(k<0 || l<0 || k>=tiles.size() || l>=tiles.size()) && tiles.get(k).get(l).getBomb()){
                    count++;
                }
                k=i-1;
                l=j;
                if(!(k<0 || l<0 || k>=tiles.size() || l>=tiles.size()) && tiles.get(k).get(l).getBomb()){
                    count++;
                }
                k=i+1;
                l=j;
                if(!(k<0 || l<0 || k>=tiles.size() || l>=tiles.size()) && tiles.get(k).get(l).getBomb()){
                    count++;
                }
                tiles.get(i).get(j).setLabel(String.valueOf(count));
                count=0;
            }
        }
    }

    /**
     * if user wins all boombs go green.
     */
    private void markBombsWin(){
        for(int i=0;i<20;i++) {
            for (int j = 0; j < 20; j++) {
                if (tiles.get(i).get(j).getBomb()) {
                    ImageView bombPic = new ImageView(bomb);
                    bombPic.setFitWidth(23);
                    bombPic.setFitHeight(23);
                    tiles.get(i).get(j).greenBomb(bombPic);
                }
            }
        }
    }

    /**
     * Checks if user won.
     * @param controller
     */
    private void checkWon(Controller controller){
        if(count==300 && controller.getPlayBtn().isDisabled()){
            timer.cancel();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("You Won\n" + "It took you: " + time + " seconds");
            controller.getTilePane().setOnMouseReleased(null);
            alert.show();
            time=0;
            markBombsWin();
            controller.getPlayBtn().setDisable(false);
        }
    }

    /**
     * Counts how many flag are on the curent board.
     * @param controller
     */
    private void checkFlags(Controller controller){
        int flags=100;
        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < 20; j++) {
                if (tiles.get(i).get(j).hasImage()) {
                    flags--;
                }
            }
        }
        controller.getBombs().setText(String.valueOf(flags));
    }

    /**
     * Sets mouse pressed on tiles to null.
     */
    private void setPressedNull(){
        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < 20; j++) {
                tiles.get(i).get(j).setOnMousePressed(null);
            }
        }
    }
}
