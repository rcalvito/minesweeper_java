package app;


import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Rectangle;
import javafx.scene.paint.Color;


/**
 * Tile class for minesweeper.
 * @author Rodrigo Calvo
 * @version 1.0
 */
public class Tile extends StackPane {
    // Variables needed for class
    private ImageView flag;
    private ImageView questionMark;
    private Rectangle rect = new Rectangle();
    private boolean bomb;
    private boolean allowed=true;
    private boolean pressed;
    private Label number= new Label("");

    /**
     * Creates new tiles
     * @param height height of tile
     * @param width width of tile
     * @param image flag image
     * @param image2 bomb image
     * @param bomb whether it has a bomb or not.
     */
    public Tile(double height, double width, Image image,Image image2,boolean bomb){
        flag= new ImageView(image);
        questionMark= new ImageView(image2);
        rect.setHeight(height);
        rect.setWidth(width);
        flag.setFitWidth(width);
        flag.setFitHeight(height);
        questionMark.setFitWidth(width);
        questionMark.setFitHeight(height);
        pressed=false;

        this.bomb=bomb;
        this.setHeight(height);
        this.setWidth(width);
        rect.setStyle("-fx-fill: lightskyblue; -fx-stroke-width: 0.5; -fx-stroke: black");
        this.getChildren().add(number);
        this.getChildren().add(rect);


        // Sets all event handlers on tile.
        this.setOnMouseEntered(event -> {
            rect.setFill(((Color) rect.getFill()).darker());
        });
        this.setOnMouseExited(event -> {
            rect.setFill(((Color) rect.getFill()).brighter());
        });

        this.setOnMousePressed(event -> {
            if (event.isSecondaryButtonDown() && allowed) {
                if (this.getChildren().contains(flag)){
                    this.getChildren().remove(flag);
                    this.getChildren().add(questionMark);
                }
                else if(this.getChildren().contains(questionMark)){
                    this.getChildren().remove(questionMark);
                }
                else {
                    this.getChildren().add(flag);
                }
            }
            else if(event.isPrimaryButtonDown()){
                if(!this.getChildren().contains(questionMark) && !this.getChildren().contains(flag)) {
                    if(bomb==true){
                        rect.setStyle("-fx-fill: red; -fx-stroke-width: 0.5; -fx-stroke: lightskyblue");
                        allowed=false;
                        this.setOnMouseEntered(null);
                        this.setOnMouseExited(null);
                    }
                    else{
                        rect.setStyle("-fx-fill: LightBlue; -fx-stroke-width: 0.5; -fx-stroke: lightskyblue");
                    }
                }
            }
        });

    }


    /**
     * @return whether it has a bomb or not.
     */
    public boolean getBomb(){
        return bomb;
    }

    /**
     * @return true if tile has been pressed.
     */
    public boolean getPressed(){
        return pressed;
    }

    /**
     * Presses a tile.
     */
    public void gotPressed(){
        rect.setStyle("-fx-fill: LightBlue; -fx-stroke-width: 0.5; -fx-stroke: lightskyblue");
        number.toFront();
        pressed=true;
        //rect.toFront();
        this.setOnMousePressed(null);
        this.setOnMouseEntered(null);
        this.setOnMouseExited(null);
        this.setOnMouseReleased(null);
    }

    /**
     * @return true if tile has a flag or question mark.
     */
    public boolean hasImage(){
        if(this.getChildren().contains(flag) || this.getChildren().contains(questionMark)){
            return true;
        }
        return false;
    }

    /**
     * Turns tile green.
     */
    public void turnGreen(){
        rect.setStyle("-fx-fill: Green; -fx-stroke-width: 0.5; -fx-stroke: lightskyblue");
    }

    /**
     * Turns tile to yellow
     */
    public void turnYellow(){
        rect.setStyle("-fx-fill: Yellow; -fx-stroke-width: 0.5; -fx-stroke: lightskyblue");
    }

    /**
     * Turns tile to red
     */
    public void turnRed(){
        rect.setStyle("-fx-fill: Red; -fx-stroke-width: 0.5; -fx-stroke: lightskyblue");

    }

    /**
     * @return true if label is 0, false otherwise.
     */
    public boolean hasLabel(){
        if(number.getText().equals("0")){
            return true;
        }
        return false;
    }

    /**
     * Sets label
     * @param content
     */
    public void setLabel(String content){
        if(!this.getBomb()) {
            number.setText(content);
        }
    }

    /**
     * Turns tile to green and adds bomb image.
     * @param image
     */
    public void greenBomb(ImageView image){
        rect.setStyle("-fx-fill: Green; -fx-stroke-width: 0.5; -fx-stroke: lightskyblue");
        this.getChildren().add(image);
    }
}


