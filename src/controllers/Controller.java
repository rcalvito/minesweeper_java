package controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.TilePane;

/**
 * Controller class for fxml
 * @author Rodrigo Calvo
 * @version 1.0
 */
public class Controller {
    // all variables
    @FXML
    private TilePane tilePane;

    @FXML
    private Button playBtn;

    @FXML
    private Label timer;

    @FXML
    private Label bombs;


    /**
     * @return tilepane
     */
    public TilePane getTilePane() {
        return tilePane;
    }

    /**
     * @return returns play button
     */
    public Button getPlayBtn() {
        return playBtn;
    }

    /**
     * @return timer label
     */
    public Label getTimer() {
        return timer;
    }

    /**
     * @return bombs label
     */
    public Label getBombs() {
        return bombs;
    }
}

